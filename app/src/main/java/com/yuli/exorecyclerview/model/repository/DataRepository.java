package com.yuli.exorecyclerview.model.repository;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.firebase.auth.FirebaseUser;
import com.yuli.exorecyclerview.model.MediaObject;
import com.yuli.exorecyclerview.model.YoutubeApi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DataRepository {
    private static final String TAG = DataRepository.class.getSimpleName();
    private static final String YOUTUBE_PLAYLIST_PART = "snippet";
    private static String appName="ExoRecyclerView";//TODO:use getResources().getString(R.string.app_name)
    //TODO:getData from youtube api
    ExecutorService ioExecutor;
    private static DataRepository instance;
    private YouTube mYoutubeDataApi;
    private final GsonFactory mJsonFactory;
    private final HttpTransport mTransport;

    public static DataRepository getInstance(Application application) {
        if (instance == null) {
            instance = new DataRepository(
                    Executors.newSingleThreadExecutor());
        }
        return instance;
    }

    public DataRepository( ExecutorService ioExecutor) {
        this.ioExecutor = ioExecutor;
        this.mJsonFactory= new GsonFactory();
        this.mTransport = new NetHttpTransport();
        this.mYoutubeDataApi = new YouTube.Builder(mTransport, mJsonFactory, null)
                .setApplicationName(appName)
                .build();
    }
    public LiveData<List<MediaObject>> fetchMediaObjectList(){
        LiveData<List<MediaObject>> list=new LiveData<List<MediaObject>>( ) {

        };
        return list;
    }
    public List<PlaylistItem> fetchPlayList(String playlistId) {
        Callable<PlaylistItemListResponse> callable =new Callable<PlaylistItemListResponse>() {
            @Override
            public PlaylistItemListResponse call() throws Exception {
                return mYoutubeDataApi.playlistItems()
                        .list(YOUTUBE_PLAYLIST_PART)
                        .setPlaylistId(playlistId)
                        .setMaxResults(Long.valueOf(10))
                        .setKey("AIzaSyDHICMLhyNHBY_MiIhhDPIxQ3SJvDBa47c").execute();
            }
        };
        try {
            PlaylistItemListResponse playlistItemListResponse=ioExecutor.submit(callable).get();
            return playlistItemListResponse.getItems();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    public  List<Channel> fetchUserChannel(FirebaseUser currentUser,String oauthToken) {
        Callable<ChannelListResponse> callable =new Callable<ChannelListResponse>() {
            @Override
            public ChannelListResponse call() throws Exception {
                return mYoutubeDataApi.channels()
                        .list(YOUTUBE_PLAYLIST_PART)
                        .setOauthToken(oauthToken)
                        .setMine(true)
                        .setMaxResults(Long.valueOf(10))
                        .setKey("AIzaSyDHICMLhyNHBY_MiIhhDPIxQ3SJvDBa47c").execute();
            }
        };
        try {
            ChannelListResponse channelListResponse=ioExecutor.submit(callable).get();
            return channelListResponse.getItems();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
    private List<MediaObject> prepareVideoList() {
        List<MediaObject> mediaObjectList=new ArrayList<>();
        MediaObject mediaObject = new MediaObject();
        mediaObject.setId(1);
        mediaObject.setUserHandle("@h.pandya");
        mediaObject.setTitle(
                "Do you think the concept of marriage will no longer exist in the future?");
        mediaObject.setCoverUrl(
                "https://androidwave.com/media/images/exo-player-in-recyclerview-in-android-1.png");
        mediaObject.setUrl("https://androidwave.com/media/androidwave-video-1.mp4");

        MediaObject mediaObject2 = new MediaObject();
        mediaObject2.setId(2);
        mediaObject2.setUserHandle("@hardik.patel");
        mediaObject2.setTitle(
                "If my future husband doesn't cook food as good as my mother should I scold him?");
        mediaObject2.setCoverUrl(
                "https://androidwave.com/media/images/exo-player-in-recyclerview-in-android-2.png");
        mediaObject2.setUrl("https://androidwave.com/media/androidwave-video-2.mp4");

        MediaObject mediaObject3 = new MediaObject();
        mediaObject3.setId(3);
        mediaObject3.setUserHandle("@arun.gandhi");
        mediaObject3.setTitle("Give your opinion about the Ayodhya temple controversy.");
        mediaObject3.setCoverUrl(
                "https://androidwave.com/media/images/exo-player-in-recyclerview-in-android-3.png");
        mediaObject3.setUrl("https://androidwave.com/media/androidwave-video-3.mp4");

        MediaObject mediaObject4 = new MediaObject();
        mediaObject4.setId(4);
        mediaObject4.setUserHandle("@sachin.patel");
        mediaObject4.setTitle("When did kama founders find sex offensive to Indian traditions");
        mediaObject4.setCoverUrl(
                "https://androidwave.com/media/images/exo-player-in-recyclerview-in-android-4.png");
        mediaObject4.setUrl("https://androidwave.com/media/androidwave-video-6.mp4");

        MediaObject mediaObject5 = new MediaObject();
        mediaObject5.setId(5);
        mediaObject5.setUserHandle("@monika.sharma");
        mediaObject5.setTitle("When did you last cry in front of someone?");
        mediaObject5.setCoverUrl(
                "https://androidwave.com/media/images/exo-player-in-recyclerview-in-android-5.png");
        mediaObject5.setUrl("https://androidwave.com/media/androidwave-video-5.mp4");

        mediaObjectList.add(mediaObject);
        mediaObjectList.add(mediaObject2);
        mediaObjectList.add(mediaObject3);
        mediaObjectList.add(mediaObject4);
        mediaObjectList.add(mediaObject5);
        mediaObjectList.add(mediaObject);
        mediaObjectList.add(mediaObject2);
        mediaObjectList.add(mediaObject3);
        mediaObjectList.add(mediaObject4);
        mediaObjectList.add(mediaObject5);
        return mediaObjectList;
    }


}
