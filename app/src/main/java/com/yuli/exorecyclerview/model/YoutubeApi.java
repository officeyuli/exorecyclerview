package com.yuli.exorecyclerview.model;

import com.google.api.services.youtube.model.ChannelListResponse;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.api.services.youtube.model.PlaylistItemListResponse;
import com.google.gson.Gson;


import java.util.Map;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

public class YoutubeApi {
    private static final String TAG="YoutubeApi";
    private static Retrofit retrofit;
    private static YoutubeApiInterface apiInterface;
    private static Gson gson;
    private static final String BASE_URL = "https://www.googleapis.com";
    private YoutubeApi(YoutubeApiInterface apiInterface) {
        YoutubeApi.apiInterface = apiInterface;
    }


    public static YoutubeApiInterface getApi(Gson gson) {
        if (apiInterface == null) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.BODY);
            OkHttpClient client = new OkHttpClient.Builder()
                    .addInterceptor(logging)
                    .build();

            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();

            apiInterface = retrofit.create(YoutubeApiInterface.class);
        }
        return apiInterface;
    }
    public static YoutubeApiInterface getApi() {
        if (apiInterface == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(new OkHttpClient())
                    .build();
            apiInterface = retrofit.create(YoutubeApiInterface.class);
        }
        return apiInterface;
    }

    public static Gson getGson() {
        return gson;
    }

    public interface YoutubeApiInterface {
        @GET("/youtube/v3/search")
        Call<ChannelListResponse> getYouTubeVideos(@Query("key") String apiKey,
                                                   @Query("channelId") String channelId,
                                                   @Query("part") String videoPart,
                                                   @Query("order") String videoOrder,
                                                   @Query("maxResults") int maxResults);
        @GET("/youtube/v3/playlistItems")
        Call<PlaylistItemListResponse> getYouTubePlaylistItems(@Query("key") String apiKey,
                                                               @Query("part") String videoPart,
                                                               @Query("playlistId") String playlistId,
                                                               @Query("maxResults") int maxResults);
    }


}