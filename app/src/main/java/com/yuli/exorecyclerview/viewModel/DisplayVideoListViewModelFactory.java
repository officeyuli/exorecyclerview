package com.yuli.exorecyclerview.viewModel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.yuli.exorecyclerview.model.repository.DataRepository;

import java.lang.reflect.InvocationTargetException;

public class DisplayVideoListViewModelFactory implements ViewModelProvider.Factory {
    private final DataRepository repository;
    public DisplayVideoListViewModelFactory(DataRepository repository) {
        this.repository = repository;
    }
    public static DisplayVideoListViewModelFactory createFactory(Application application) {
        if (application == null) {
            throw new IllegalStateException("Application not ready");
        }
        return new DisplayVideoListViewModelFactory(
                DataRepository.getInstance(application));
    }
    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        try {
            return modelClass.getConstructor(DataRepository.class)
                    .newInstance(repository);
        } catch (IllegalAccessException | InstantiationException |
                InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            throw new RuntimeException("Failed to create " + modelClass);
        }
    }
}
