package com.yuli.exorecyclerview.viewModel;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.PlaylistItem;
import com.google.firebase.auth.FirebaseUser;
import com.yuli.exorecyclerview.model.MediaObject;
import com.yuli.exorecyclerview.model.repository.DataRepository;

import java.util.List;

public class DisplayVideoListViewModel extends ViewModel {
    private static final String TAG = DisplayVideoListViewModel.class.getSimpleName();
    private final DataRepository repository;
    MutableLiveData<List<PlaylistItem>> playlistItems;
    MutableLiveData<List<Channel>> channelList;
    public DisplayVideoListViewModel(DataRepository repository) {
        this.repository = repository;
        this.playlistItems=new MutableLiveData<>();
        this.channelList=new MutableLiveData<>();
    }
    public MutableLiveData<List<PlaylistItem>> getPlaylistItems(){return playlistItems;}
    public void fetchPlaylistItems(String listId){
        playlistItems.setValue(repository.fetchPlayList(listId));
    }
    public MutableLiveData<List<Channel>> getChannelList(){return channelList;}
    public void fetchUserChannel(FirebaseUser currentUser,String oauthToken) {
        channelList.setValue(repository.fetchUserChannel(currentUser,oauthToken));
    }
}
